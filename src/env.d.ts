/// <reference types="vite/client" />

/* eslint-disable */
declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<{}, {}, any>
  export default component
}

import { Route, Router } from 'vue-router'
declare module 'vue' {
  interface ComponentCustomOptions {
    beforeRouteEnter?(to: Route, from: Route, next: () => void): void
  }
}

import 'pinia'
import { Repository } from '@/utils'
declare module 'pinia' {
  export interface PiniaCustomProperties {
    api: Repository
    router: Router
  }
}
