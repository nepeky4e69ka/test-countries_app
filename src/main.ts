import { createApp } from 'vue';
import { createPinia } from 'pinia';
import { api } from '@/utils';
import { components } from '@/utils/components';
import router from '@/utils/router';
import App from '@/components/App.vue';
import './style/index.scss';

createApp(App)
  .use(router)
  .use(components)
  .use(createPinia().use(() => ({ api, router })))
  .mount('#app');
