import { defineStore } from 'pinia';
import { defaultState, defaultCountry } from './countries.defaults';
import { RouteNames } from '@/utils';
import { ParamsGet, ParamsSearch } from '@/api';
import {
  KEY,
  CountryBorder,
  CountriesState,
  CountriesActionNames as CAN,
} from './countries.interface';

export const useCountriesStore = defineStore(KEY, {
  state: (): CountriesState => defaultState(),
  getters: {
    empty: (state: CountriesState) => !state.loading && !state.countries.length,
    countryBorders: (state: CountriesState): CountryBorder[] =>
      state.country.borders?.map((alpha3Code) => ({
        params: { alpha3Code },
        title: state.codes[alpha3Code],
        name: RouteNames.COUNTRY,
      })) || [],
  },
  actions: {
    async [CAN.GET_COUNTRIES](): Promise<void> {
      this.loading = true;
      const api = this.api.countries[CAN.GET_COUNTRIES];
      const { skip, limit, name } = this;
      const { data } = await api({ skip, limit, name });
      this.countries = data.data;
      this.loading = false;
    },
    async [CAN.GET_COUNTRY]({ alpha3Code }: ParamsGet): Promise<void> {
      try {
        this.loading = true;
        const api = this.api.countries[CAN.GET_COUNTRY];
        const { data } = await api({ alpha3Code });
        this.country = data.data;
        this.loading = false;
      } catch (e) {
        this.router.push({ name: RouteNames.INDEX });
      }
    },
    async [CAN.GET_CODES](): Promise<void> {
      const api = this.api.countries[CAN.GET_CODES];
      const { data } = await api();
      this.codes = data.data;
    },
    [CAN.SET_DEFAULT](): void {
      this.country = defaultCountry();
      this.loading = true;
    },
    [CAN.SET_NAME]({ name }: ParamsSearch): void {
      this.name = name;
    },
  },
});
