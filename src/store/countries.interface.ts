export const KEY = 'countries';
export interface CountriesState {
  countries: Country[];
  country: Country;
  codes: Codes;
  total: number;
  page: number;
  limit: number;
  skip: number;
  loading: boolean;
  name: string;
}
export interface Codes {
  [c: string]: string;
}
export interface Language {
  iso639_1: string;
  iso639_2: string;
  name: string;
  nativeName: string;
}
export interface Currency {
  code: string;
  name: string;
  symbol: string;
}
export interface Country {
  name: string;
  topLevelDomain: string[];
  alpha3Code: string;
  capital: string;
  subregion: string;
  region: string;
  population: number;
  borders: string[];
  nativeName: string;
  currencies: Currency[];
  languages: Language[];
  flag: string;
}

export interface CountryBorder {
  name: string;
  params: { alpha3Code: string };
  title: string;
}

export enum CountriesActionNames {
  GET_COUNTRIES = 'getCountries',
  GET_COUNTRY = 'getCountry',
  GET_CODES = 'getCodes',
  SET_DEFAULT = 'setDefault',
  SET_NAME = 'setName',
}
