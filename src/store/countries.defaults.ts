import { CountriesState, Country } from '@/store';

export const defaultState = (): CountriesState => ({
  total: 0,
  page: 1,
  limit: 48,
  skip: 0,
  loading: true,
  name: '',
  countries: [],
  codes: {},
  country: defaultCountry(),
});

export const defaultCountry = (): Country => ({
  name: '',
  topLevelDomain: [],
  alpha3Code: '',
  capital: '',
  subregion: '',
  region: '',
  population: 0,
  borders: [],
  nativeName: '',
  currencies: [],
  languages: [],
  flag: '',
});
