import { MockFunction, MockHandler } from 'vite-plugin-mock-server';
import { reduce, search, find, json, aSleep } from '@/utils';
import { API, StatusCode as SC } from '@/api';
import { Country } from '@/store';
import { join } from 'path';
import { readFileSync } from 'fs';
/**
 *
 * @returns City[]
 */
const createDb = () => {
  const path = join(__dirname, 'data.json');
  const data = readFileSync(path, 'utf8');
  const db = JSON.parse(data) as Country[];
  return reduce(db).sort((a, b) => a.name.localeCompare(b.name));
};
/**
 * Simulate DB
 */
const fakeDB: Country[] = createDb();
/**
 * @function handleGet
 * @param req
 * @param res
 */
const handleCountries: MockFunction = async (req, res) => {
  const skip = req.query?.skip ? parseInt(req.query?.skip) : 0;
  const limit = req.query?.limit ? parseInt(req.query?.limit) : 40;
  const name = req.query?.name;
  const end = limit + skip;
  const db = name ? search(name, fakeDB) : fakeDB;
  const data = db.slice(skip, end);
  const response = JSON.stringify({ data });
  // simulate res
  await aSleep(500);
  res.setHeader('content-type', json);
  res.end(response);
};
//
const handleCountry: MockFunction = async (req, res) => {
  const code = req.body?.alpha3Code;
  if (!code) {
    res.statusCode = SC.BAD_REQUEST;
    res.end();
    return;
  }
  const data = find(code, fakeDB);
  if (!data) {
    res.statusCode = SC.NOT_FOUND;
    res.end();
    return;
  }
  const response = JSON.stringify({ data });
  // simulate res
  await aSleep(500);
  res.setHeader('content-type', json);
  res.end(response);
};
/**
 * @returns Codes
 */
const handleCodes: MockFunction = async (req, res) => {
  const data = fakeDB.reduce((a, v) => ({ ...a, [v.alpha3Code]: v.name }), {});
  const response = JSON.stringify({ data });
  res.setHeader('content-type', json);
  res.end(response);
  void req;
};

/**
 * routes
 */
export default (): MockHandler[] => [
  {
    pattern: API.COUNTRIES,
    method: 'GET',
    handle: handleCountries,
  },
  {
    pattern: API.COUNTRY,
    method: 'POST',
    handle: handleCountry,
  },
  {
    pattern: API.CODES,
    method: 'GET',
    handle: handleCodes,
  },
];
