import mockServer from 'vite-plugin-mock-server';
import bodyParser from 'body-parser';
export const server = mockServer({
  logLevel: 'off',
  mockRootDir: './src/mock',
  middlewares: [
    bodyParser.json(),
    bodyParser.urlencoded(),
    bodyParser.text(),
    bodyParser.raw(),
  ],
});
