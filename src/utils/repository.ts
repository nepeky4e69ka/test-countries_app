import { axiosInstance as $axios } from './axios';
import { CountriesRepository, countriesRepository } from '@/api';

export type Repository = {
  countries: CountriesRepository;
};

const createRepository = (): Repository => {
  return {
    countries: countriesRepository($axios),
  };
};

export const api = createRepository();
