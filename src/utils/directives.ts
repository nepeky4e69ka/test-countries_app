import type { Directive } from 'vue';
/**
 * @implements directive for lazy-load image
 */
const lazyImage: Directive = {
  mounted(el) {
    try {
      function handler(entries: IntersectionObserverEntry[]) {
        const { isIntersecting, target } = entries[0];
        if (isIntersecting) {
          const image = target as HTMLImageElement;
          if (image.dataset.src) {
            image.src = image.dataset.src;
            image.classList.add('-loaded');
            observer.unobserve(el);
          }
        }
      }
      const observer = new IntersectionObserver(handler);
      observer.observe(el);
    } catch (e) {
      void e;
    }
  },
};

export { lazyImage };
