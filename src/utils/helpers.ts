import { Country } from '@/store';
export const json = 'application/json; charset=utf-8';
export const aSleep = (ms: number) =>
  new Promise((resolve) => setTimeout(resolve, ms));

export const search = (input: string, countries: Country[]): Country[] =>
  countries.filter(({ name }) =>
    name.toLowerCase().includes(input.toLowerCase()),
  );

export const find = (code: string, countries: Country[]): Country | undefined =>
  countries.find(({ alpha3Code }) => alpha3Code === code);

export const formatNumber = (n: number): string =>
  n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
/**
 * @function reduce data size
 * @param countries <Country[]>
 * @returns {Country[]}
 */
export const reduce = (countries: Country[]) =>
  countries.map(
    ({
      name,
      topLevelDomain,
      alpha3Code,
      capital,
      subregion,
      region,
      population,
      borders,
      nativeName,
      currencies,
      languages,
      flag,
    }) => ({
      name,
      topLevelDomain,
      alpha3Code,
      capital,
      subregion,
      region,
      population,
      borders,
      nativeName,
      currencies,
      languages,
      flag,
    }),
  );
