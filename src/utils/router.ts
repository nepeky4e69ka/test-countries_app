import { createRouter, createWebHistory } from 'vue-router';
import { RouteNames } from '@/utils';
export const routes = [
  {
    path: '/',
    name: RouteNames.INDEX,
    component: () => import('@/components/pages/Index.vue'),
  },
  {
    path: '/:alpha3Code',
    name: RouteNames.COUNTRY,
    component: () => import('@/components/pages/Country.vue'),
  },
];

const history = createWebHistory();
export default createRouter({
  history,
  routes,
});
