/**
 * @constant USER_API url provided from .env
 */
export const config = {
  USER_API: '/', //import.meta.env?.VITE_USER_API || '/',
};

export enum RouteNames {
  INDEX = 'index',
  COUNTRY = 'country',
}

export enum DirectiveName {
  LAZY_IMAGE = 'lazy-image',
}
