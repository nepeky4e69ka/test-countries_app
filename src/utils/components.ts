import { DirectiveName as DN, lazyImage } from '@/utils';
import type { App } from 'vue';
// components
import countryDetail from '@/components/country/detail.vue';
import countryItem from '@/components/country/item.vue';
import countryList from '@/components/country/list.vue';
// UI
import uiLoading from '@/components/ui/loading.vue';
import uiHeader from '@/components/ui/header.vue';
import uiSearch from '@/components/ui/search.vue';
import uiBack from '@/components/ui/back.vue';
import uiImage from '@/components/ui/image.vue';
import uiEmpty from '@/components/ui/empty.vue';
// icons
import iArrowLeft from '@/components/icons/arrow-left.vue';
import iSearch from '@/components/icons/search.vue';
/**
 * @function components register components
 * @description for auto import use 'unplugin-vue-components/vite' or glob
 * @param app
 */
export const components = (app: App): App => {
  app.directive(DN.LAZY_IMAGE, lazyImage);
  // components
  app.component('c-detail', countryDetail);
  app.component('c-item', countryItem);
  app.component('c-list', countryList);
  // ui
  app.component('ui-empty', uiEmpty);
  app.component('ui-image', uiImage);
  app.component('ui-loading', uiLoading);
  app.component('ui-header', uiHeader);
  app.component('ui-search', uiSearch);
  app.component('ui-back', uiBack);
  // icons
  app.component('i-arrow-left', iArrowLeft);
  app.component('i-search', iSearch);
  return app;
};
