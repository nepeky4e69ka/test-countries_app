import { AxiosInstance } from 'axios';
import { CountriesActionNames as CAN } from '@/store';
import {
  API,
  CountryResponse,
  ParamsGet,
  ParamsSearch,
  CodesResponse,
  CountriesResponse,
} from './countries.interface';

export const countriesRepository = (axios: AxiosInstance) => ({
  [CAN.GET_COUNTRIES](params: ParamsSearch) {
    return axios.get<CountriesResponse>(API.COUNTRIES, { params });
  },
  [CAN.GET_COUNTRY](body: ParamsGet) {
    return axios.post<CountryResponse>(API.COUNTRY, body);
  },
  [CAN.GET_CODES]() {
    return axios.get<CodesResponse>(API.CODES);
  },
});

export type CountriesRepository = ReturnType<typeof countriesRepository>;
