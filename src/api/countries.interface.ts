import { Country, Codes } from '@/store';
import { AxiosResponse } from 'axios';
import { RouteLocationNormalized } from 'vue-router';
export interface Params {
  skip?: number;
  limit?: number;
}
export interface ParamsSearch extends Params {
  name: string;
}
export interface ParamsGet {
  alpha3Code: string;
}
export interface CountriesResponse extends AxiosResponse<Country[]> {
  total: number;
}
export type CountryResponse = AxiosResponse<Country>;
export type CodesResponse = AxiosResponse<Codes>;

export enum API {
  COUNTRIES = '/api/countries',
  COUNTRY = '/api/country',
  CODES = '/api/codes',
}

export enum StatusCode {
  BAD_REQUEST = 400,
  NOT_FOUND = 404,
  SUCCESS = 200,
}

export interface RouteTo extends Omit<RouteLocationNormalized, 'params'> {
  params: ParamsGet;
}
